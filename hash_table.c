#include <stdlib.h>
#include <string.h>
#include "hash_table.h"

void init_array(struct dynamic_array* darr)
{
    if(darr == NULL)
        return;

    darr->size = 0;
    darr->capacity = 0;
    darr->records = NULL;
}

void free_array(struct dynamic_array* darr)
{
    if(darr == NULL)
        return;

    free(darr->records);
    darr->size = 0;
    darr->capacity = 0;
    darr->records = NULL;
}

void add_record(struct dynamic_array* darr, const struct dht_record* rec)
{
    if(darr == NULL || rec == NULL)  //verify parameters
        return;

    if(darr->records == NULL)
    {
        darr->capacity = 2;
        darr->records = (struct dht_record*)malloc(sizeof(struct dht_record) * darr->capacity);
        darr->size = 0;
    }
    else
    {
        // Check if present
        struct dht_record* duplicate = find_record(darr, rec->hash);
        if(duplicate != NULL)
        {
             strcpy(duplicate->associated_address, rec->associated_address);
             return;
        }

        if(darr->size == darr->capacity)
        {
            darr->capacity = darr->capacity * 2;
            struct dht_record* new_records = (struct dht_record*)malloc(sizeof(struct dht_record) * darr->capacity);
            int i = 0;
            for(i = 0; i < darr->size; i++)
            {
                memcpy(&new_records[i], &darr->records[i], sizeof(struct dht_record));
            }
            free(darr->records);
            darr->records = new_records;
        }
    }
    memcpy(&darr->records[darr->size], rec, sizeof(struct dht_record));
    darr->size++;
}

void remove_record(struct dynamic_array* darr, const struct dht_record* rec)
{
    if(darr == NULL || rec == NULL)
        return;

    int i = 0;
    int idx_found = -1;//not found

    for(i = 0; i < darr->size; i++)
    {
        if(strcmp(darr->records[i].hash, rec->hash) == 0)// matching hashes?
        {
            idx_found = i;
            break;
        }
    }
    //
    if(idx_found != -1)//if match -> delete it
    {
        memcpy(&darr->records[idx_found], &darr->records[darr->size - 1], sizeof(struct dht_record));
        darr->size--;
    }
}

struct dht_record* find_record(struct dynamic_array* darr, const char* hash)
{
    struct dht_record* res = NULL;
    if(darr == NULL || hash == NULL)
        return res;
    
    int i = 0;
    for(i=0; i < darr->size; i++)
    {
        if(strcmp(darr->records[i].hash, hash) == 0)//if match we found it 
        {
            res = &darr->records[i];
            break;
        }
    }
    return res;
}