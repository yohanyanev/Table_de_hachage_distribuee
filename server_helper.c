#include "server_helper.h"
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "common_defs.h"

#define KEEP_ALIVE_TIMEOUT 8
#define HASH_OBSOLETE_TIMEOUT 15

void send_sync_msg(int sockfd, struct sockaddr_in6* addr, const struct dht_record* req, MSG_ID sync_type)
{
    if(addr == NULL || req == NULL)
    {
        LOG("Can't send sync data, wrong data!\n");
        return;
    }

    LOG("Sending sync for %s\n", req->hash);

    struct dht_sync_msg fwd_msg;
    socklen_t addrlen = sizeof(struct sockaddr_in6);
    fwd_msg.msg_id = sync_type;
    memcpy(&fwd_msg.data, req, sizeof(fwd_msg.data));

    if(sendto(sockfd,(struct dht_sync_msg*)&fwd_msg,(sizeof(struct dht_sync_msg)),0,
             (struct sockaddr *) addr, addrlen) == -1)
    {
        LOG("Can't send sync message\n");
    }
}

void handle_put_request_msg(const struct dht_request* req, struct dynamic_array* darr)
{
    if(req == NULL || darr == NULL)
    {
        LOG("Can't handle put request msg, wrong data!");
        return;
    }
    LOG("Received request hash = [%s]\n", req->hash);

    struct dht_record rec;
    strcpy(rec.hash, req->hash);
    strcpy(rec.associated_address, req->ip);
    rec.last_request_time = time(0);
    add_record(darr, &rec);
}

void handle_get_request_msg(const struct dht_request* req,
                            struct dynamic_array* darr,
                            int sockfd,
                            struct sockaddr_in6* addr)
{
    struct dht_response response;
    response.is_present = FALSE;
    response.ip[0] = '\0';

    struct dht_record* rec = find_record(darr, req->hash);
    if(rec != NULL)
    {
        response.is_present = TRUE;
        strcpy(response.ip, rec->associated_address);
        rec->last_request_time = time(0);
    }

    socklen_t addrlen = sizeof(struct sockaddr_in6);
    if(sendto(sockfd,(struct dht_response*)&response,(sizeof(response)),0,
    (struct sockaddr *) addr, addrlen) == -1)
    {
        LOG("Can't respond to request");
    }
}

void handle_sync_msg(const struct dht_record* rec, struct dynamic_array* darr, MSG_ID sync_type)
{
    if(sync_type == DHT_SYNC_ADD_MSG_ID)
    {
        add_record(darr, rec);
    }
}

void send_keep_alive_msg(int sockfd, struct server_list* list)
{
   //LOG("keep alive\n");
    socklen_t addrlen = sizeof(struct sockaddr_in6);
    struct keep_alive_msg msg;
    msg.msg_id = KEEP_ALIVE_MSG_ID;

    int i;
    for(i = 0; i < list->size; i++)
    {
        if (sendto(sockfd,(struct keep_alive_msg*)&msg,(sizeof(msg)),0,
            (struct sockaddr*) &list->servers[i].address, addrlen) == -1)
        {
            LOG("Can't send keep alive\n");
        }
    }
}

void check_backup_servers(struct server_list* list)
{
    if(list == NULL)
    {
        LOG("Can't check keep alive, invalid clients list!\n");
        return;
    }
    time_t now = time(0);

    int i = 0;
    for(i = 0; i < list->size; i++)
    {
        double diff = difftime(now, list->servers[i].last_alive_check);
        if(diff >= KEEP_ALIVE_TIMEOUT)
        {
            remove_server(list, &list->servers[i]);
            i--;
        }
    }

}

void update_keep_alive(struct server_list* list, struct sockaddr_in6* add)
{
    struct server cl;
    memcpy(&cl.address, add, sizeof(cl.address));
    struct server* to_update = find_server(list, &cl);
    if(to_update != NULL)
    {
        to_update->last_alive_check = time(0);
    }
    else
    {
        LOG("Not found\n");
    }
}

void garbage_collect_hashes(struct dynamic_array* darr)
{
    if(darr == NULL)
    {
        LOG("Can't garbage  hashes,collect  data");
        return;
    }

    int i;
    time_t now = time(0);
    for(i = 0; i < darr->size; i++)
    {
        double diff = difftime(now, darr->records[i].last_request_time);
        if(diff > HASH_OBSOLETE_TIMEOUT)
        {
            LOG("Removing record with id %s\n", darr->records[i].hash);
            remove_record(darr, &darr->records[i]);
            i--;
        }
    }
}