#ifndef SERVER_LIST_H
#define SERVER_LIST_H

#include <sys/socket.h>
#include <netinet/in.h>
#include <time.h>

/*@brief Structure qui contient  l'adresse d'un serveur
*/
struct server
{
    struct sockaddr_in6 address;
    time_t last_alive_check;
};

/**
* @brief Structure qui contient tous les serveurs connectés
**/
struct server_list
{
    int size;
    int capacity;
    struct server* servers;
};


/** @brief init_array initialise une liste dynamic au debut á 0
 *  @param struct server_list* liste en question
 **/
void init_server_list(struct server_list*);

/** @brief free_server_list supprime la liste  (met tous à 0 état initial)
 *  @param sstruct server_list*   tab dynamic en question
 **/
void free_server_list(struct server_list*);

/** @brief add_server ajoute un serveur dans la liste
 *  @param struct server_list*  liste dynamic en question
 *  @param  const struct server* //le serveur garder dans la liste
 **/
void add_server(struct server_list*, const struct server*);

/** @brief remove_server supprime un serveur de la liste
 *  @param struct server_list  liste dynamic en question
 *  @param  const struct server * le serveur a supprimé
 **/
void remove_server(struct server_list*, const struct server*);

/** @brief find_server cherche serveur dans la liste 
 *  @param struct server_list* la liste avec les serveurs
 *  @return dht_serveur le serveur cherché
 **/
struct server* find_server(struct server_list*, const struct server*);

#endif // SERVER_LIST_H