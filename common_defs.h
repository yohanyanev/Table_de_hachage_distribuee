#ifndef COMMON_DEFS_H
#define COMMON_DEFS_H

#include <time.h>
#include <stdio.h>

#define HASH_LEN 60
#define IP_ADDRESS_LEN 128
#define COMMAND_LEN 3

typedef int MSG_ID;
#define DHT_REQUEST_MSG_ID 1   //msg server - client
#define AUTHORISATION_MSG_ID 2 //server connect to server 
#define DHT_SYNC_ADD_MSG_ID 3 //msg server - server /ajoute client data/
#define KEEP_ALIVE_MSG_ID 4 //msg server - server /keep alive/

//creation de type bool 
enum BOOL
{
    TRUE = 0,
    FALSE
}__attribute__((__packed__));

/** @brief La struct dht_record représente 
 * les données qui sont gardées dans un tableau dynamic.
 * last_request_time pour suivre le temp de derniere acces.(test hash obsoletes)
 **/
struct dht_record
{
    char hash[HASH_LEN+1];
    char associated_address[IP_ADDRESS_LEN+1];
    time_t last_request_time;
} __attribute__((__packed__));

/** @brief Represente les donnees d'une demande de client
 *  @param char * hash un hash avec longeur hash_len+1
 *  @param char * cmd la commande indiqué par le client
 *  @param char * ip l'adress ip de client qui sera garder dans le tab
 **/
struct dht_request
{
    char hash[HASH_LEN+1];
    char cmd[COMMAND_LEN+1];
    char ip[IP_ADDRESS_LEN+1];
} __attribute__((__packed__));

/** @brief Struct dht_response : represente la reponse  envoye par le serveur au client
 *  @param enum BOOL is_present : indique ,si le hash est present dans le tab ou pas
 *  @param ip[IP_ADDRESS_LEN+1] : l'adesse ip associée au hash demandé
 **/
struct dht_response
{
     enum BOOL is_present;
     char ip[IP_ADDRESS_LEN+1];
} __attribute__((__packed__));


/** @brief Le message envoyer par le client au serveur
 *  @param MSG_ID msg_id identifiant de message pour pouvoir distinguer
 *  les messages de clients vers le serveur et les message de serveur vers un
 *  autre serveur.les dht_request sont representes par le numero 1
 *  @param struct dht_request data.
 **/
struct dht_request_msg //message type 1
{
    MSG_ID msg_id;
    struct dht_request data;
} __attribute__((__packed__));

/*La structure de messges envoyees entre deux ou plusieurs serveurs
 Quand le serveur detecte des donnés passe de client /dht_request_msg/
 il envoye au ces serveurs connectes un message de type dht_sync_msg */
struct dht_sync_msg // messasge type 3
{
    MSG_ID msg_id;
    struct dht_record data;
} __attribute__((__packed__));


enum authorisation_type 
{
    CLIENT = 0,
    SERVER,
}__attribute__((__packed__));

struct authorisation_dscr
{
    enum authorisation_type auth_type;
}__attribute__((__packed__));

/** @brief Represente un message envoye par un serveur a un autre srv
 *         afin d'etre reconnu come serveur et pas comme client.
 * le msg_id ici est egal a 2 
 **/
struct authorisation_msg  //message type 2
{
    MSG_ID msg_id;
    struct authorisation_dscr data;
} __attribute__((__packed__));

//envoye un message simple pour indique que le backupserver est tjs present
struct keep_alive_msg
{
    MSG_ID msg_id;
} __attribute__((__packed__));

//Permet tres facilement d'activer ou de désactiver les messages output
#define ENABLE_LOGS

#ifdef ENABLE_LOGS
#define LOG(...) printf(__VA_ARGS__)
#else
#define LOG(...)
#endif

#endif // COMMON_DEFS_H