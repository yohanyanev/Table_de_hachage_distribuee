TABLE DE HACHAGE DISTRIBUEE (DHT)
=================================
	Ce projet cree une bibliotheque de gestion d'une table de hachage distribuee (DHT), 
	qui utilise uniquement le protocole UDP. 

	
REALISATION
===========
	Pour la realisation de ce projet, j'ai cree 6 fichiers supplementaires en plus de client.c et server.c
   (hash_table.c/h , common_defs.h , network_utils.c/h , server_helper.c/h , server_messages.h, server_list.c/h)

  ### 1.Hash_table.c/h
  ---------------
   
	Le dht est realise a l'aide d'un tableau dynamique qui utilise le fichier common_defs.h 
	Dans le tableau sont gardees des donnees de type const struct dht_record* (voir common_defs.h)
	Cette structure contient : char hash , char associated_adress et une variable last_request_time qui
	fait suivre la duree de la derniere utilisation du record. Cela est utile pour verifier 
	si un hash est devenu obsolete.
    
  ### 2.Common_defs.h
  ---------------
    Ce fichier contient toutes les structures necessaires pour le fonctionement du serveur et du client.
    Il y a 4 types de messages differents qui circulent entre les serveurs et le client.
    Etant donne que ce projet utilise udp, il faut un moyen de distinguer ces messages.
    J'ai choisi de representer chacun de ces messages avec un numero d'identification allant de 1 a 4.
	
    ~~TYPE DE MESSAGES~~
   
	- Le premier message est defini avec l'id 1 (DHT_REQUEST_MSG_ID).
     Ce message permet de distinguer la communication entre 
     client et serveur. La structure qui definit ce message strcut dht_request_msg, prend comme arguments 
	 le numero id 1 et une structure dht_request qui contient un hash, une adresse ip et une commande.
	 La structure reponse dht_reponse contient seulement l'adresse ip envoyee par le serveur 
	 pour optimiser l'utilisation de la memoire. Cette structure est utilisee seulement 
	 au cas ou le serveur recoit une commande GET d'un client.
	
	- Le deuxieme message, qui porte le numero d'id 2 (AUTHORISATION_MSG_ID), indique a un serveur 
	 qu'il y un autre serveur qui s'est connecte a lui.	
	
	- Le troisieme type de message, ayant pour id 3 (DHT_SYNC_ADD_MSG_ID), aide le serveur a 
	 transferer les messages passes d'un client, au serveur connecte a lui.
	 La structure qui definit ce message, struct dht_sync_msg, 
	 prend l'id, et la structure dht_record contient le message du client.
	
	- Le dernier type de message (KEEP_ALIVE_MSG_ID) permet de garder la connection ouverte entre deux serveurs.
	 Quand l'un de ces serveurs tombe en panne ou se deconnecte, l'envoi de ce message est interrompu.
	 La structure qui definit ce message, struct keep_alive_msg, prend seulement le numero id.
	
	~~~~~~~~~~~~~~~~~~ 
	
	Pour m'assurer que chaque structure conserve sa taille avant et apres l'envoi sur Internet, 
	j'utilise une instruction de preprocesseur __attribute__((__packed__));

  ### 3.Server_messages.h
  -------------------
	Ici j'ai cree une union de toutes ces structures (struct msg_union) pour faciliter le fonctionement du serveur.
	Cette nouvelle structure prend en charge tous les messages possibles.

  ### 4.Server_helper.c/h
  ------------------
    Le serveur utilise un switch case sur le numero d'identification des messages. 
	Apres chaque fonctionnement du serveur, en fonction du numero d'identification des messages, est place
	dans le fichier server_helper.c pour ameliorer la lisibilite de code.
	Les fonctions sont definies comme suit :
	
	- handle_put_request_msg() qui gere les message de type DHT_REQUEST_MSG_ID
	
	- send_sync_ms() qui envoie une message de type DHT_SYNC_ADD_MSG_ID au serveur connecte.
	
	- handle_put_request_msg() gere les messages recus par un client de type DHT_REQUEST_MSG_ID.
	                            Si un backup serveur est detecte, alors la requete est transferee.
	
	- handle_sync_msg() gere les messages de type DHT_SYNC_ADD_MSG_ID recus par un autre serveur.
	
	- send_keep_alive_msg() envoie un message de type KEEP_ALIVE_MSG_ID aux serveurs pour garder la connection ouverte.
	
	- void check_backup_server() verifie si les serveurs de la liste des serveurs (voir server_list) sont restes connectes. 
										 Si deux serveurs ne s'echangent pas de messages durant plus de KEEP_ALIVE_TIMEOUT secondes, 
                                         alors la connection entre eux est consideree comme ferme.
    
	- void update_keep_alive()  Mise a jour de timer. Si un message keep_alive recu, alors le timer s'annule.
									
    - garbage_collect_hashes() verifie si un hash est devenu obsolete. La condition pour cela est que si un hash dans le tableau
	                           ne recoit pas de commande PUT ou GET dans HASH_OBSOLETE_TIMEOUT, alors le hash est supprime.
	
	
  ### 5.network_utils.c/h
  -------------------
	Ce fichier contient deux fonctions communes pour le serveur et le client
	host2ip() qui trouve l'adresse ip a partir d un nom
	error() affiche des messages d'erreur et retour un code d'erreur 1

 ### 6.server_list.c/h
 ---------------------
        Ce fichier est influance par le fichier 1has_table.c/h  et il cree une liste dynamique avec tous les serveur connectes
	
CHOIX POUR CETTE IMPLEMENTATION
===============================
	J'ai cree ces differentes structures pour optimiser l'utilisation de la memoire. Comme cela en fonction des besoins, 
	chaque programme prendra la structure optimale. Par exemple si un serveur recoit la commande GET, il n'envoie pas au client la structure struct msg_union contenant tous les messages au client, mais seulement la structure reponse qui contient l'adresse ip associee.

OPTIMISATION
============
	Pour les messages dans la console j'ai utilise des LOG permettant facilement  
        de les desactiver sans modifier chaque fichier.	
	
	
	
	
	
	