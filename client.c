#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include "common_defs.h"
#include "network_utils.h"
#include "server_helper.h"


int main(int argc, char * argv[])
{
    //3 arg sont obligatoire
    if(argc < 4)
    {
        error("usage:./client + Server IPv6 + Server port + cmd + Hash + [IPv6]\n");
    }

    struct sockaddr_in6 echoServAddr;
    struct dht_request_msg clientData;
    socklen_t addrlen;
    int sockfd;
    char * host_ip;

    if((sockfd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        error("socket creation error");
        return 1;
    }

    //make socket non-blocking, 5 ms timeout
    struct timeval read_timeout;
    read_timeout.tv_sec = 5;
    read_timeout.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof(read_timeout));

    memset(&echoServAddr,0,sizeof(echoServAddr));
    echoServAddr.sin6_family = AF_INET6;
    echoServAddr.sin6_port   = htons(atoi(argv[2]));
    addrlen                  = sizeof(struct sockaddr_in6);

    host_ip = host2ip((argv[1]));

    clientData.msg_id = DHT_REQUEST_MSG_ID; //type client-server

    if(strcmp(argv[3], "PUT") == 0)
    {
        if(argv[5]==NULL || argv[4]==NULL )
        {
            error("usage: cmd PUT + hash + IPv6\n");
        }
        strncpy(clientData.data.hash,    argv[4], HASH_LEN);
        strncpy(clientData.data.cmd,  argv[3], COMMAND_LEN);
        strncpy(clientData.data.ip,argv[5], IP_ADDRESS_LEN);
    }
    else if(strcmp(argv[3], "GET") == 0)
    {
        if(argv[4] == NULL)
        {
            error("usage: cmd GET + hash \n");
        }
        strncpy(clientData.data.hash,  argv[4], HASH_LEN);
        strncpy(clientData.data.cmd,argv[3], COMMAND_LEN);
    }

    if((inet_pton(AF_INET6, host_ip, &(echoServAddr.sin6_addr))) != 1)
    {
        close(sockfd);
        error("inet_pton() clientfailed\n");
    }//sting to binary

    if (sendto(sockfd,(struct dht_request*)&clientData,(sizeof(clientData)),0,
               (struct sockaddr *) &echoServAddr,addrlen) == -1)
    {
        close(sockfd);
        error("Error while sending request to udpserver");
    }
    else//si message bien envoyé  , I am waiting for reply
    {
        if(strcmp(argv[3], "GET") == 0)//only if GET
        {
            // wait for message
            struct dht_response response;
            if((recvfrom(sockfd, &response, sizeof(response),0,
                         (struct sockaddr *) &echoServAddr, &addrlen)) < 0)
            {
                printf("Error while recieving anwser of GET request\n");
            }
            else
            {
                if(response.is_present == TRUE)// if hash available  print it
                {
                    printf("IP disponibles pour le hash %s : [%s]\n",clientData.data.hash, response.ip);
                }
                else //if not some sad messsage
                {
                    printf("Hash is not available on server\n");
                }
            }
        }
    }
    close(sockfd);
    free(host_ip);
    return 0;
}
