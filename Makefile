 SHELL=/bin/sh
 CFLAGS= -Wall -g
 CC=gcc
 CLIENT_SRC= client.c network_utils.c hash_table.c
 SERVER_SRC= server.c network_utils.c hash_table.c server_helper.c server_list.c
 
  all: client server
  .PHONY: all
  
  .PHONY: client
 client:
	${CC} ${CFLAGS} -o client ${CLIENT_SRC}

  .PHONY: server
 server:
	${CC} ${CFLAGS} -o server ${SERVER_SRC}
 
 .PHONY: clean
 clean:
	rm -f *.o client server 
