#ifndef SERVER_MESSAGES
#define SERVER_MESSAGES

#include "common_defs.h"

/** @brief Union de ts les messages possibles dans une seule structure
 **/
struct msg_union
{
    MSG_ID msg_id;
    union
    {
      struct dht_request dht_req;
      struct authorisation_dscr auth_msg;
      struct dht_record sync_msg;
    } messages;
} __attribute__((__packed__));//specifies that a type must have the smallest possible alignment.

#endif // SERVER_MESSAGES