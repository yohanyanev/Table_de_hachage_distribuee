#ifndef HASH_TABLE_H
#define HASH_TABLE_H
#include "common_defs.h"

/** @brief dynamic_array contenant  les hash avec leurs ip associés
 *  @param int size la taille actuelle du tableau
 *  @param int capacity le capacité  du tableau dynamic
 *  @param dht_record * records contenant ip et hash (voir common_defs.h)
 **/
struct dynamic_array
{
    int size;
    int capacity;
    struct dht_record* records;
};

/** @brief init_array initialise le tableau dynamic au debut á 0
 *  @param struct dynamic_array* darr  tab dynamic en question
 **/
void init_array(struct dynamic_array*);

/** @brief free_array supprime le tableau  (met tous à 0 état initial)
 *  @param struct dynamic_array* darr  tab dynamic en question
 **/
void free_array(struct dynamic_array*);

/** @brief add_record ajoute un donné dans le tableau
 *  @param struct dynamic_array* darr  tab dynamic en question
 *  @param  const struct dht_recird * //contenant ip et hash
 **/
void add_record(struct dynamic_array*, const struct dht_record*);

/** @brief remove_record supprime un donne dans le tableau
 *  @param struct dynamic_array* darr  tab dynamic en question
 *  @param  const struct dht_recird * le donne a supprimé
 **/
void remove_record(struct dynamic_array*, const struct dht_record*);

/** @brief find_record cherche un ip a partir de hash 
 *  @param struct dynamic_array* darr  tab dynamic en question
 *  @param  const char * //le hash qui cherche son ip associé dans le tab
 *  @return dht_record * // le donne contenant la couple hash/ip
 **/
struct dht_record* find_record(struct dynamic_array*, const char*);

#endif // HASH_TABLE_H