#ifndef NETWORK_UTILS_H_
#define NETWORK_UTILS_H

/**@brief Fonction host2ip IP host resolver
 *  @param const char * host nom de hote passe en parametre
 * @return char * ip correspondant a cet hote.
 **/
char* host2ip(const char * host);

/**@brief Fonction error  simple error handler
 *  @param const char * msg // l'error imprimable sur IO
 **/
void error(char * msg);


#endif //NETWORK_UTILS_H