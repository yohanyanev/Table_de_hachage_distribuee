#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "network_utils.h"

void error(char * msg)
{
  perror(msg);
  exit(1);
}

char * host2ip(const char * host)
{
  char * addr_buf;
  addr_buf=malloc(64*sizeof(char));
  struct addrinfo* feed_server = NULL;
  struct addrinfo hints;
  memset (&hints, 0, sizeof (hints));
  memset(addr_buf, 0,sizeof(64));
  hints.ai_family = AF_INET6;
  hints.ai_socktype = SOCK_STREAM;

  getaddrinfo(host, NULL, &hints, &feed_server);
  struct addrinfo *res;
  for(res = feed_server; res != NULL; res = res->ai_next)
  {   
    if( res->ai_family == AF_INET6 )
    {
      inet_ntop(AF_INET6, &((struct sockaddr_in6 *)res->ai_addr)->sin6_addr, 
                addr_buf, sizeof(addr_buf));
    }
  }
  return (char *)addr_buf;
}
