#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/select.h>//use select() for multiplexing
#include <sys/fcntl.h> // for non-blocking
#include <time.h>
#include <unistd.h>
#include "hash_table.h"
#include "common_defs.h"
#include "network_utils.h"
#include "server_messages.h"
#include "server_helper.h"
#include "server_list.h"


int main(int argc, char * argv[])
{   
    if(argc < 3)
    {
        error("usage: ./server + addrs + PORT\n");
    }
    if(atoi(argv[2]) < 0)
    {
        error("usage: port can't be negative number\n");
    }

    enum BOOL has_backup_server = FALSE;
    if(argc == 5)
    {
        has_backup_server = TRUE;
    }
  
    int sockfd,recvfd;
    char * host_ip;
    socklen_t outAddrLen,inAddrLen;
    struct sockaddr_in6 serveraddr,clientaddr;
      
    //Server specifications
    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin6_family = AF_INET6;
    serveraddr.sin6_port = htons(atoi(argv[2]));
    host_ip = host2ip(argv[1]);
    inet_pton(AF_INET6, host_ip, &(serveraddr.sin6_addr));

    char ip6[INET6_ADDRSTRLEN];
    inet_ntop(AF_INET6, &(serveraddr.sin6_addr), ip6, INET6_ADDRSTRLEN);
    printf("Server address is: %s\n", ip6);
    printf("Server port number: %d\n",ntohs(serveraddr.sin6_port));

    sockfd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
    if(sockfd == -1)
    {
        error("Socket creation error\n");
    }
    printf("Server-socket OK...\n");


    // associate socket/address
    outAddrLen=sizeof(serveraddr);
    if( bind(sockfd,(struct sockaddr *)&serveraddr,outAddrLen) < 0 )
    {
        error("Bind error\n");
    }
    else
    {
        printf("Server-bind OK...\n");
        printf("Waiting for incoming connection.\n");
    }

    //make socket non-blocking, 10 ms timeout
    struct timeval read_timeout;
    read_timeout.tv_sec = 1;
    read_timeout.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &read_timeout, sizeof(read_timeout));
    struct server_list sr_list;
    init_server_list(&sr_list);
  
    struct sockaddr_in6 backup_server_addr;

    if(has_backup_server == TRUE)
    {
        memset(&backup_server_addr, 0, sizeof(backup_server_addr));
        socklen_t addrlen;
        backup_server_addr.sin6_family = AF_INET6;
        backup_server_addr.sin6_port   = htons(atoi(argv[4]));
        addrlen                        = sizeof(struct sockaddr_in6);

        host_ip = host2ip((argv[3]));

        if((inet_pton(AF_INET6, host_ip, &(backup_server_addr.sin6_addr))) != 1)
        {
            close(sockfd);
            error("inet_pton() clientfailed\n");
        }//string to binary

        struct authorisation_msg auth_msg;
        //type authentication:server
        auth_msg.msg_id = AUTHORISATION_MSG_ID;
        auth_msg.data.auth_type = SERVER;
        printf("sending authentication msg\n");

        if(sendto(sockfd,(struct authorisation_msg*)&auth_msg,(sizeof(auth_msg)),0,
                  (struct sockaddr *) &backup_server_addr,addrlen) == -1)
        {
            error(" Can't send handshake message to main server");
        }
        
        struct server to_add;
        to_add.last_alive_check = time(0);
        memcpy(&to_add.address, &backup_server_addr, sizeof(backup_server_addr));
        add_server(&sr_list, &to_add);
    }
    ////////////////////////////////////////////////////////////////////////////
    struct msg_union msgs;
    struct dynamic_array darr;
    init_array(&darr);
    time_t last_alive_send = time(0);
    ////////////////////////////////////////////////////////////////////////////
    for(;;)
    {
        inAddrLen = sizeof(clientaddr);
        msgs.msg_id = 0;
        if((recvfd = recvfrom(sockfd, &msgs, sizeof(struct msg_union),0,
                              (struct sockaddr *) &clientaddr, &inAddrLen)) < 0)
        {
            //continue;
        }

        inet_ntop(AF_INET6, &clientaddr.sin6_addr,ip6,sizeof(ip6));
        switch(msgs.msg_id)
        {
            case DHT_REQUEST_MSG_ID : //message type : client-server
            {
                if(strcmp(msgs.messages.dht_req.cmd, "PUT") == 0)
                {
                    handle_put_request_msg(&msgs.messages.dht_req, &darr);
                    int i;
                    struct dht_record* to_sync = find_record(&darr, msgs.messages.dht_req.hash);
                    for(i = 0; i < sr_list.size; i++)
                    {
                        send_sync_msg(sockfd,&sr_list.servers[i].address, to_sync, DHT_SYNC_ADD_MSG_ID);
                    }
                }         
                else if(strcmp(msgs.messages.dht_req.cmd, "GET") == 0)
                {
                    handle_get_request_msg(&msgs.messages.dht_req, &darr, sockfd, &clientaddr);
                }
            }   
            break;
                    
            case AUTHORISATION_MSG_ID :
                LOG("Received auth msg\n");
                struct server to_add;
                to_add.last_alive_check = time(0);
                memcpy(&to_add.address, &clientaddr, sizeof(clientaddr));
                add_server(&sr_list, &to_add);
            break;

            case DHT_SYNC_ADD_MSG_ID :
                LOG("Sync add received %s\n", msgs.messages.sync_msg.hash);
                handle_sync_msg(&msgs.messages.sync_msg, &darr, DHT_SYNC_ADD_MSG_ID);
            break;


            case KEEP_ALIVE_MSG_ID :
                //LOG("Keep alive received\n");
                update_keep_alive(&sr_list, &clientaddr);
            break;

            default :
            break;
        }//end switch

        time_t now = time(0);
        double diff = difftime(now, last_alive_send);
        if(diff > 1)
        {
            send_keep_alive_msg(sockfd, &sr_list);
            last_alive_send = now;
        }
        check_backup_servers(&sr_list);
        garbage_collect_hashes(&darr);

    }//end while
  ////////////////////////////////////////////////////////////////////////////////
  close(sockfd);
  free(host_ip);
  free_array(&darr);
  free_server_list(&sr_list);
  return 0;
}
