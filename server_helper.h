#ifndef SERVER_HELPER_H
#define SERVER_HELPER_H

#include <sys/socket.h>
#include <netinet/in.h>
#include "hash_table.h"
#include "common_defs.h"
#include "server_list.h"


/** @brief Fonction qui envoye un message de synchronisation entre deux serveurs.
 *         Il y a deux type de message de sync :
 *          (1.ajoute de donne passe d'un client
 *           2.message keep_alive )
 *  @param const struct sockaddr_in6* l'adresse de backupserver
 *  @param struct dht_record* pointeur vers 
 *         le record recu par client et transmit au backupserver
 * @param MSG_ID sync_type indique au backupserver le type de message 
 **/
void send_sync_msg(int, struct sockaddr_in6*, const struct dht_record*, MSG_ID sync_type);

/** @brief Fonction qui gere les messages
 *   passées de client au serveur  avec la commande PUT
 *  @param const struct dht_request* le messege request envoyée'par le client
 *  @param struct dynamic_array* pointeur vers 
 *         le tableau dynamic ou on va garder les données
 **/
void handle_put_request_msg(const struct dht_request*, struct dynamic_array*);

/** @brief Fonction qui gére les messages
 *         passées de client au serveur  avec la commande GET
 *  @param const struct dht_request* le messege request envoyée'par le client
 *  @param struct dynamic_array* pointeur vers
 *         le tableau dynamic ou on va chercher les adress ip associées
 *  @param int fd le socked oú dht_reponse*** est envoyée.(***definit dans common_defs.h)
 *  @param struct sockaddr_in6 * addr contient l'adress de client
 **/
void handle_get_request_msg(const struct dht_request*, struct dynamic_array*, int, struct sockaddr_in6*);


/** @brief Fonctuon qui gere les messages de sync
 *  @param const struct dynamic_array* pointeur vers un tableau dynamic ou sont garder les donnes
 *  @param struct dht_record* pointeur vers le record recu
 * @param MSG_ID sync_type  pour verifier le type de message (il y a deux types de message server-server)
 **/
void handle_sync_msg(const struct dht_record*, struct dynamic_array*, MSG_ID sync_type);

/** @brief Fonction qui énvoye un message keeplive au serveurs connectés. 
 *         Permet de savoir si un serveur est deconecté ou en panne
 *  @param int fd le socked oú le message est evnoyé
 *  @param struct sserver_list*  la liste des serveurs
 **/
void send_keep_alive_msg(int, struct server_list*);

/** @brief Fonction qui gere le timeout.(30 sec par défaut) 
 *  @param struct server_list* une liste de tous le serveurs connectés
 **/
void check_backup_servers(struct server_list*);

/**
 *  @brief Mise a jour de message keep_alive pour chaque serveur dans la liste
 *         (Si keep alive recu, timer a 0)
 * @param struct server_list* liste qui contient tous les serveurs connectes
 * @param struct sockaddr_in6 ladresse de serveur
 **/
void update_keep_alive(struct server_list*, struct sockaddr_in6*);


/** @brief recupere les hashes obsoletes et les detruire
 *  @param dynamic_array* pointeur qui garders les donnes(hashes + ip)
 *  @#param struct sockaddr_in6*
 **/
void garbage_collect_hashes(struct dynamic_array*);

#endif // SERVER_HELPER_H
