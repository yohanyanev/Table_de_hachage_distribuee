#include "server_list.h"
#include "common_defs.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void init_server_list(struct server_list* list)
{
    if(list == NULL)
    {
        LOG("Cannot init servers list\n");
        return;
    }

    list->size = 0;
    list->capacity = 0;
    list->servers = NULL;
}

void free_server_list(struct server_list* list)
{
    if(list == NULL)
    {
        LOG("Invalid list for free\n");
        return;
    }

    free(list->servers);
    list->size = 0;
    list->capacity = 0;
    list->servers = NULL;
}

void add_server(struct server_list* list, const struct server* cl)
{
    if(list == NULL || cl == NULL)
    {
        LOG("Can't add server to list\n");
        return;
    }

    if(list->servers == NULL)
    {
        list->capacity = 2;
        list->servers = (struct server*)malloc(sizeof(struct server) * list->capacity);
        list->size = 0;
    }
    else
    {
        struct server* dup = find_server(list, cl);
        if(dup != NULL)
        {
            LOG("server already in list\n");
            return;
        }

        if(list->size == list->capacity)
        {
            list->capacity = list->capacity * 2;
            struct server* new_servers = (struct server*)malloc(sizeof(struct server) * list->capacity);
            int i = 0;
            for(i = 0; i < list->size; i++)
            {
                memcpy(&new_servers[i], &list->servers[i], sizeof(struct server));
            }
            free(list->servers);
            list->servers = new_servers;
        }
    }
    memcpy(&list->servers[list->size], cl, sizeof(struct server));
    list->size++;
}

#define IP6_ADR_LEN 16
static enum BOOL same_addresses(const struct sockaddr_in6* l, const struct sockaddr_in6* r)
{
    int i = 0;
    for(i = 0; i < IP6_ADR_LEN; i++)
    {
        if (l->sin6_addr.s6_addr[i] != r->sin6_addr.s6_addr[i])
        {
            return FALSE;
        }
    }
    //
    if(l->sin6_port != r->sin6_port)
    {
        return FALSE;
    }
    //
    return TRUE;
}

void remove_server(struct server_list* list, const struct server* cl)
{

    if(list == NULL || cl == NULL)
    {
        LOG("Error while removing server\n");
        return;
    }

    int i = 0;
    int idx_found = -1;

    for(i = 0; i < list->size; i++)
    {
        if(same_addresses(&list->servers[i].address, &cl->address) == TRUE)
        {
            idx_found = i;
            break;
        }
    }
    //
    if(idx_found != -1)
    {
        memcpy(&list->servers[idx_found], &list->servers[list->size - 1], sizeof(struct server));
        list->size--;
    }
}

struct server* find_server(struct server_list* list, const struct server* cl)
{
    struct server* res = NULL;
    if(list == NULL || cl == NULL)
    {
        LOG("Can't find server in list\n");
        return res;
    }
    
    int i = 0;
    for(i=0; i < list->size; i++)
    {
        if(same_addresses(&list->servers[i].address, &cl->address) == TRUE)
        {
            res = &list->servers[i];
            break;
        }
    }
    return res;
}